#!/bin/bash
if [ "$TRAVIS_OS_NAME" == "osx" ]; then
  sudo rm -rf /Library/Developer/CommandLineTools
  ansible --version
  molecule test
fi