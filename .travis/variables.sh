if [ "$TRAVIS_OS_NAME" == "osx" ]; then
  source $HOME/.rvm/scripts/rvm
  rvm reload
  export PATH="$HOME/.pyenv/versions/$PYTHON/bin:${PATH}"
  export PATH="$HOME/.rvm/rubies/ruby-$RUBY/bin:${PATH}"
fi