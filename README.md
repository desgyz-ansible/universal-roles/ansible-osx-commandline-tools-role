Ansible Role: osx-command-line-tools
====================================

[![Travis](https://img.shields.io/travis/desgyz/ansible-osx-commandline-tools-role.svg?style=flat-square)](https://travis-ci.org/desgyz/ansible-osx-commandline-tools-role)
[![MIT licensed][mit-badge]][mit-link]
[![Ansible Role](https://img.shields.io/ansible/role/d/27478.svg?style=flat-square)](https://galaxy.ansible.com/desgyz/ansible_osx_commandline_tools_role)

An Ansible role that installs OS X Command Line Tools.

This role is an exact copy of [ansible-osx-command-line-tools](https://github.com/elliotweiser/ansible-osx-command-line-tools) by [Elliot Weiser](https://github.com/elliotweiser) the only difference is I merged some of the pending Pull Requests, integrated Molecule with TestInfra for Continous Integration and attempted to fix some pending issues. The purpose of this repository was for me to learn about how to develop Continous Integration for Ansible roles using Molecule and TravisCI while also giving a Role I personally use some TLC.

Requirements
------------

None (except running on Mac OS X).

Role Variables
--------------

Going forward, all role variables will be prefixed with `clt_` (for **Command Line Tools**) to avoid ambiguity and namespace collision.

| Variable Name | Description | Default |
| :--- | :--- |  :---:  |
| `clt_force_install` | Install the Command Line Tools, even if already installed |  `no`   |
| `clt_softwareupdate_list_timeout` | How long (in seconds) to wait for an available Command Line Tools package |  `300`  |
| `clt_softwareupdate_install_timeout` | How long (in seconds) to wait for installation of the Command Line Tools  |  `1200` |

Dependencies
------------

None.

License
-------

[MIT][mit-link]

Author Information
------------------

[Elliot Weiser](https://github.com/elliotweiser)

Maintainer(s)
------------------

- [Audi Bailey (Desgyz)](https://github.com/desgyz)

[mit-badge]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[mit-link]: https://raw.githubusercontent.com/desgyz/ansible-homebrew/master/LICENSE